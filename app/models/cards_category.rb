class CardsCategory < ApplicationRecord

    validates_uniqueness_of :card_value
    validates_uniqueness_of :order

end
