module Api
  module V1
    class AddressesController < ApplicationController
      before_action :set_address, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

      # GET list of all addresses and display it
      def index
        @addresses = Address.all
        respond_to do |format|
          format.json { render json: @addresses }
        end

        # =begin
        # @api {get} /api/v1/addresses 1-Request addresses List
        # @apiVersion 0.3.0
        # @apiName GetAddress
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/addresses
        # @apiSuccess {Number} id address unique ID (Created automatically).
        # @apiSuccess {Number} user_id the user unique id.
        # @apiSuccess {String} address the detailed address.
        # @apiSuccess {String} country the user address country.
        # @apiSuccess {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiSuccess {String} city the user address city.
        # @apiSuccess {String} street the user address street.
        # @apiSuccess {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "address": "New Damiatta , el-mahgob,service high",
        #       "country": "EG",
        #       "governorate": "Damiatta",
        #       "city": "New Damiatta",
        #       "street": "el-mahgob",
        #       "default_address": false,
        #       "created_at": "2018-09-12T10:48:29.946Z",
        #       "updated_at": "2018-09-12T10:48:29.946Z"
        #   },
        #   {
        #       "id": 2,
        #       "user_id": 2,
        #       "address": "المنصوره طلخا دقهليه",
        #       "country": "EG",
        #       "governorate": "دقهلية",
        #       "city": "طلخا",
        #       "street": "شارع البحر الاعظم",
        #       "default_address": true,
        #       "created_at": "2018-09-12T10:54:22.468Z",
        #       "updated_at": "2018-09-12T11:28:17.326Z"
        #   }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # GET a spacific address and display it
      def show

        respond_to do |format|
          format.json { render json: @address }
        end

        # =begin
        # @api {get} /api/v1/addresses/{:id} 2-Request Specific Address
        # @apiVersion 0.3.0
        # @apiName GetSpecificAddress
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/addresses/1
        # @apiParam {Number} id address unique ID.
        # @apiSuccess {Number} id address unique ID (Created automatically).
        # @apiSuccess {Number} user_id the user unique id.
        # @apiSuccess {String} address the detailed address.
        # @apiSuccess {String} country the user address country.
        # @apiSuccess {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiSuccess {String} city the user address city.
        # @apiSuccess {String} street the user address street.
        # @apiSuccess {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 1,
        #   "user_id": 1,
        #   "address": "New Damiatta , el-mahgob,service high",
        #   "country": "EG",
        #   "governorate": "Damiatta",
        #   "city": "New Damiatta",
        #   "street": "el-mahgob",
        #   "default_address": false,
        #   "created_at": "2018-09-12T10:48:29.946Z",
        #   "updated_at": "2018-09-12T10:48:29.946Z"
        # }
        # @apiError AddressNotFound The id of the address was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "AddressNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # GET a list of spacific user addresses
      def user_addresses

        @addresses = Address.where(:user_id => 1).all
        respond_to do |format|
          format.json { render json: @addresses }
        end

        # =begin
        # @api {get} /api/v1/user_addresses 3-Request Specific User Addresses
        # @apiVersion 0.3.0
        # @apiName GetSpecificUserAddresses
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/user_addresses
        # @apiParam {Number} user_id the user unique id.
        # @apiSuccess {Number} id address unique ID (Created automatically).
        # @apiSuccess {Number} user_id the user unique id.
        # @apiSuccess {String} address the detailed address.
        # @apiSuccess {String} country the user address country.
        # @apiSuccess {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiSuccess {String} city the user address city.
        # @apiSuccess {String} street the user address street.
        # @apiSuccess {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "address": "New Damiatta , el-mahgob,service high",
        #       "country": "EG",
        #       "governorate": "Damiatta",
        #       "city": "New Damiatta",
        #       "street": "el-mahgob",
        #       "default_address": false,
        #       "created_at": "2018-09-12T10:48:29.946Z",
        #       "updated_at": "2018-09-12T10:48:29.946Z"
        #   },
        #   {
        #       "id": 5,
        #       "user_id": 1,
        #       "address": "جده شارع خالد ابن الوليد",
        #       "country": "SA",
        #       "governorate": "جده",
        #       "city": "الدخيله",
        #       "street": "شارع خالد ابن الوليد",
        #       "default_address": false,
        #       "created_at": "2018-09-12T11:15:30.747Z",
        #       "updated_at": "2018-09-12T11:15:30.747Z"
        #   }
        # ]
        # @apiError UserNotFound The id of the user was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "UserNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # GET a new address
      def new
        @address = Address.new
      end

      # GET an existing address and edit params
      def edit
      end

      # POST a new address and save it
      def create


        # =begin
        # @api {post} /api/v1/addresses 4-Create a new address
        # @apiVersion 0.3.0
        # @apiName PostAddress
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/addresses \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "user_id": 3,
        #   "address": "mansoura,talkha",
        #   "country": "EG",
        #   "governorate": "Daqahliyah",
        #   "city": "mansoura",
        #   "street": "talkha,om kalthom street",
        #   "default_address": false
        # }'
        # @apiParam {Number} user_id the user unique id.
        # @apiParam {String} address the detailed address.
        # @apiParam {String} country the user address country.
        # @apiParam {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiParam {String} city the user address city.
        # @apiParam {String} street the user address street.
        # @apiParam {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Number} id address unique ID (Created automatically).
        # @apiSuccess {Number} user_id the user unique id.
        # @apiSuccess {String} address the detailed address.
        # @apiSuccess {String} country the user address country.
        # @apiSuccess {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiSuccess {String} city the user address city.
        # @apiSuccess {String} street the user address street.
        # @apiSuccess {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 7,
        #         "user_id": 3,
        #         "address": "mansoura,talkha",
        #         "country": "EG",
        #         "governorate": "Daqahliyah",
        #         "city": "mansoura",
        #         "street": "talkha,om kalthom street",
        #         "default_address": false,
        #         "created_at": "2018-09-12T11:15:30.747Z",
        #         "updated_at": "2018-09-12T11:15:30.747Z"
        #     }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError InvalidAddress address must be more or equal than 5 letters.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 Invalid Address
        #   {
        #     "error": "must be more or equal than 5 letters"
        #   }
        # =end

        @address = Address.new(address_params)
        respond_to do |format|
          if @address.save
            format.json { render json: @address, status: :created, location: @address }
          else
            format.json { render json: @address.errors, status: :unprocessable_entity }
          end
        end

      end

      # Change an existing address params(address,country,governorate,governorate,street,default_address)
      def update

        respond_to do |format|
          if @address.update(address_params)
            format.json { render json: @address, status: :ok, location: @address }
          else
            format.json { render json: @address.errors, status: :unprocessable_entity }
          end
        end

        # =begin
        # @api {Put} /api/v1/addresses{:id} 5-Update an existing address
        # @apiVersion 0.3.0
        # @apiName PutAddress
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/v1/addresses/7 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "address": "mansoura,talkha",
        #   "country": "EG",
        #   "governorate": "Daqahliyah",
        #   "city": "mansoura",
        #   "street": "talkha,om kalthom street",
        #   "default_address": true
        # }'
        # @apiParam {String} address the detailed address.
        # @apiParam {String} country the user address country.
        # @apiParam {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiParam {String} city the user address city.
        # @apiParam {String} street the user address street.
        # @apiParam {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Number} id address unique ID (Created automatically).
        # @apiSuccess {Number} user_id the user unique id.
        # @apiSuccess {String} address the detailed address.
        # @apiSuccess {String} country the user address country.
        # @apiSuccess {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiSuccess {String} city the user address city.
        # @apiSuccess {String} street the user address street.
        # @apiSuccess {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 7,
        #         "user_id": 3,
        #         "address": "mansoura,talkha",
        #         "country": "EG",
        #         "governorate": "Daqahliyah",
        #         "city": "mansoura",
        #         "street": "talkha,om kalthom street",
        #         "default_address": true,
        #         "created_at": "2018-09-12T11:15:30.747Z",
        #         "updated_at": "2018-09-12T13:14:21.335Z"
        #     }
        # @apiError AddressNotFound The id of the address was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "AddressNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError InvalidAddress address must be more or equal than 5 letters.
        # @apiErrorExample Error-Response3:
        # HTTP/1.1 Invalid Address
        #   {
        #     "error": "must be more or equal than 5 letters"
        #   }
        # =end

      end

      def search

        # =begin 
        # @api {get} /api/v1/search_address?&search={:search}&search_user={:user_id}&address_status={:default_address}&searchdatefrom={:created_at_from}&searchdateto={:created_at_to} 6-Search for an existing Address
        # @apiVersion 0.3.0
        # @apiName GetAddressSearch
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/search_address?=&search=100&search_user=1&address_status=1&searchdatefrom=2018-09-11&searchdateto=2018-09-13
        # @apiParam {String} search search word that you want to find(address,country,governorate,city,street).
        # @apiParam {Date} searchdatefrom search created at date from.
        # @apiParam {Date} searchdateto search created at date to.
        # @apiParam {Boolean} address_status search default address status than you want(1 for Defualt , 0 for Not Default).
        # @apiParam {Integer} search_user search user ID than you want.
        # @apiSuccess {Number} id address unique ID.
        # @apiSuccess {Number} user_id the user unique id.
        # @apiSuccess {String} address the detailed address.
        # @apiSuccess {String} country the user address country.
        # @apiSuccess {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiSuccess {String} city the user address city.
        # @apiSuccess {String} street the user address street.
        # @apiSuccess {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "address": "New Damiatta , el-mahgob,service high",
        #       "country": "EG",
        #       "governorate": "Damiatta",
        #       "city": "New Damiatta",
        #       "street": "el-mahgob",
        #       "default_address": false,
        #       "created_at": "2018-09-12T10:48:29.946Z",
        #       "updated_at": "2018-09-12T10:48:29.946Z"
        #   }
        # @apiError NoResults There are no results for your search. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 No Results
        #   {
        #     "error": "There are no results for your search."
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        @search = params[:search]
        @searchdatefrom = params[:searchdatefrom]
        @searchdateto = params[:searchdateto]
        @address_status = params[:address_status]
        @search_user = params[:search_user]

        @addresses = Address.all
        @addresses = @addresses.where("addresses.created_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
        @addresses = @addresses.where("addresses.created_at <= ? ", @searchdateto)   if  @searchdateto.present?
        @addresses = @addresses.where("addresses.created_at <= ? AND addresses.created_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
        @addresses = @addresses.where(["address LIKE  ? OR country LIKE  ? OR governorate = ? OR city = ? OR street = ?","%#{@search}%",@search,@search,@search,"%#{@search}%"]).distinct.all if  @search.present?
        @addresses = @addresses.where("addresses.default_address = ? ", @address_status)   if  @address_status.present?
        @addresses = @addresses.where("addresses.user_id = ? ", @search_user)   if  @search_user.present?

        respond_to do |format|
          if @addresses != nil || @addresses != []
            format.json { render json: @addresses }
          else
            flash[:error] = "There are no results for your search."
            format.json { render json: flash }
          end
        end
    
      end
    
      def searchpost
      end

      # DELETE an existing address
      def destroy
        @address.destroy
        flash[:success]= "Address was successfully destroyed"
        respond_to do |format|
          format.json { render json: flash }
        end

        # =begin
        # @api {Delete} /api/v1/addresses{:id} 7-Delete an existing address
        # @apiVersion 0.3.0
        # @apiName DeleteAddress
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -X DELETE \
        # http://localhost:3000/api/v1/addresses/4 \
        # @apiParam {Number} user_id the user unique id.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #     "success": "Address was successfully destroyed"
        #   }
        # @apiError AddressNotFound The id of the address was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "AddressNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # DELETE all addresses of a specific user
      def delete_user_addresses
        @user_id = params[:user_id]
        @addresses = Address.where(:user_id => @user_id).all
        respond_to do |format|
          if @addresses != []
            @addresses.destroy_all
            format.json { render json: @addresses }
          else
            flash[:error] = "This user was not found in our database"
            format.json { render json: flash }
          end
        end

        # =begin 
        # @api {Get} /api/v1/delete_user_addresses?{:user_id} 8-DELETE all addresses of a specific user
        # @apiVersion 0.3.0
        # @apiName DeleteUserAddresses
        # @apiGroup Addresses
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/delete_user_addresses?1
        # @apiParam {Integer} user_id user ID than you want to delete all his addresses.
        # @apiSuccess {Number} id address unique ID.
        # @apiSuccess {Number} user_id the user unique id.
        # @apiSuccess {String} address the detailed address.
        # @apiSuccess {String} country the user address country.
        # @apiSuccess {String} governorate the user address governorate.expired_at  card expired Date.
        # @apiSuccess {String} city the user address city.
        # @apiSuccess {String} street the user address street.
        # @apiSuccess {Boolean} default_address defualt address for a user(true , false).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "user_id": 1,
        #       "address": "New Damiatta , el-mahgob,service high",
        #       "country": "EG",
        #       "governorate": "Damiatta",
        #       "city": "New Damiatta",
        #       "street": "el-mahgob",
        #       "default_address": false,
        #       "created_at": "2018-09-12T10:48:29.946Z",
        #       "updated_at": "2018-09-12T10:48:29.946Z"
        #   },
        #   {
        #       "id": 5,
        #       "user_id": 1,
        #       "address": "جده شارع خالد ابن الوليد",
        #       "country": "SA",
        #       "governorate": "جده",
        #       "city": "الدخيله",
        #       "street": "شارع خالد ابن الوليد",
        #       "default_address": false,
        #       "created_at": "2018-09-12T11:15:30.747Z",
        #       "updated_at": "2018-09-12T11:15:30.747Z"
        #   }
        # ]
        # @apiError UserNotFound There are no results for this user id. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 User Not Found
        #   {
        #     "error": "This user was not found in our database"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end
      

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_address
          @address = Address.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def address_params
          params.require(:address).permit(:user_id, :address, :country, :governorate, :city, :street, :default_address)
        end
    end
  end
end
