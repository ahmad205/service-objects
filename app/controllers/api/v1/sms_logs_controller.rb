module Api
  module V1
    class SmsLogsController < ApplicationController

        # GET list of SMS Logs
        def index
          @sms_logs = SmsLog.all
          respond_to do |format|
            format.json { render json: @sms_logs }
          end

          # =begin
          # @api {get} /api/v1/sms_logs 1-Request SMS Logs List
          # @apiVersion 0.3.0
          # @apiName GetSMSLogs
          # @apiGroup SMS Logs
          # @apiExample Example usage:
          # curl -i http://localhost:3000/api/v1/sms_logs
          # @apiSuccess {Number} user_id The receiver user id.
          # @apiSuccess {String} pinid SMS return pinId that was coming from the SMS provider.
          # @apiSuccess {Number} sms_type SMS type (send_pin,verify_code,text_sms).
          # @apiSuccess {Boolean} status SMS status(true = Verified , false = Not Verified).
          # @apiSuccess {Date} created_at  Date Sent.
          # @apiSuccess {Date} updated_at  Date Updated.
          # @apiSuccessExample Success-Response:
          # HTTP/1.1 200 OK
          # [
          #  {
          #     "id": 1,
          #     "user_id": 1,
          #     "pinid": "43E94670D7190DBA62CA83492F56F365",
          #     "sms_type": 1,
          #     "status": false,
          #     "created_at": "2018-08-25T12:49:45.190Z",
          #     "updated_at": "2018-08-25T12:49:45.190Z"
          #  },
          #  {
          #     "id": 2,
          #     "user_id": 1,
          #     "pinid": "A3D4C9447DFDD6F92332DDFE46A22367",
          #     "sms_type": 1,
          #     "status": true,
          #     "created_at": "2018-08-25T12:52:27.287Z",
          #     "updated_at": "2018-08-25T12:52:34.378Z"
          #  }
          # ]
          # @apiError MissingToken invalid Token.
          # @apiErrorExample Error-Response:
          # HTTP/1.1 400 Bad Request
          #   {
          #     "error": "Missing token"
          #   }
          # =end
        end

    end
  end
end