module Api
  module V1
    class UserWalletsController < ApplicationController
      before_action :set_user_wallet, only: [:show]
      skip_before_action :verify_authenticity_token

      # GET /user_wallets.json
      def index
        @user_wallets = UserWallet.all
        respond_to do |format|
          format.json { render json: @user_wallets }
        end

        # =begin
        # @api {get} /api/v1/user_wallets 1-Request Users Wallets List
        # @apiVersion 0.3.0
        # @apiName GetUserWallets
        # @apiGroup Users Wallets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/user_wallets
        # @apiSuccess {Number} id User Wallet unique ID.
        # @apiSuccess {String} currency User Wallet Currency.
        # @apiSuccess {Float} amount User Wallet amount.
        # @apiSuccess {Number} user_id Wallet User ID.
        # @apiSuccess {Number} status User Wallet (0 for Disabled & 1 for Enabled).
        # @apiSuccess {String} uuid User Wallet Secure random number (Created automatically and must be 12 digits and letters).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "currency": "USD",
        #       "amount": 1000,
        #       "user_id": 1,
        #       "status": 1,
        #       "uuid": "0fa993bcfa59",
        #       "created_at": "2018-08-02T11:03:14.634Z",
        #       "updated_at": "2018-08-02T13:51:18.183Z"
        #   },
        #   {
        #       "id": 2,
        #       "currency": "USD",
        #       "amount": 500,
        #       "user_id": 2,
        #       "status": 1,
        #       "uuid": "0b51db6a5988",
        #       "created_at": "2018-08-02T11:03:52.375Z",
        #       "updated_at": "2018-08-02T13:51:18.179Z"
        #   }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end
      end

      # GET /user_wallets/1
      def show
        # =begin
        # @api {get} /api/user_wallets/{:id} 2-Request Specific User Wallet
        # @apiVersion 0.3.0
        # @apiName GetSpecificUserWallet
        # @apiGroup Users Wallets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/user_wallets/1
        # @apiParam {Number} id Card Operation ID.
        # @apiSuccess {Number} id User Wallet unique ID.
        # @apiSuccess {String} currency User Wallet Currency.
        # @apiSuccess {Float} amount User Wallet amount.
        # @apiSuccess {Number} user_id Wallet User ID.
        # @apiSuccess {Number} status User Wallet (0 for Disabled & 1 for Enabled).
        # @apiSuccess {String} uuid User Wallet Secure random number (Created automatically and must be 12 digits and letters).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #       "id": 1,
        #       "currency": "USD",
        #       "amount": 1000,
        #       "user_id": 1,
        #       "status": 1,
        #       "uuid": "0fa993bcfa59",
        #       "created_at": "2018-08-02T11:03:14.634Z",
        #       "updated_at": "2018-08-02T13:51:18.183Z"
        #   }
        # @apiError WalletNotFound The id of this User Wallet was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Wallet Not Found"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        flash[:error]= "User Wallet Not Found"
        respond_to do |format|
          if @user_wallet
            format.json { render json: @user_wallet }
          else
            format.json { render json: flash }
          end
        end

      end

      # GET /user_wallets/new
      def new
        @user_wallet = UserWallet.new
      end

      # POST /user_wallets.json
      def create

        # =begin
        # @api {post} /api/v1/user_wallets 3-Create a new User Wallet
        # @apiVersion 0.3.0
        # @apiName PostUserWallet
        # @apiGroup Users Wallets
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/user_wallets \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        # "amount": "500",
        # "user_id": 3,
        # "status": 1
        # }'
        # @apiParam {Float} amount User Wallet amount.
        # @apiParam {Number} user_id Wallet User ID (unique).
        # @apiParam {Number} status User Wallet (0 for Disabled & 1 for Enabled).
        # @apiSuccess {Number} id User Wallet unique ID.
        # @apiSuccess {String} currency User Wallet Currency.
        # @apiSuccess {Float} amount User Wallet amount.
        # @apiSuccess {Number} user_id Wallet User ID.
        # @apiSuccess {Number} status User Wallet (0 for Disabled & 1 for Enabled).
        # @apiSuccess {String} uuid User Wallet Secure random number (Created automatically and must be 12 digits and letters).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #       "id": 3,
        #       "currency": "USD",
        #       "amount": 500,
        #       "user_id": 3,
        #       "status": 1,
        #       "uuid": "0fa663bcsa47",
        #       "created_at": "2018-08-02T15:03:04.333Z",
        #       "updated_at": "2018-08-02T15:03:04.383Z"
        #   }
        # @apiError ExistingUserId User Id has already been taken.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 Existing User Id
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError MissingToken invalid token .
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        @user_wallet = UserWallet.new(user_wallet_params)

        respond_to do |format|
          if @user_wallet.save
            format.json { render json: @user_wallet, status: :created, location: @user_wallet }
          else
            format.json { render json: @user_wallet.errors, status: :unprocessable_entity }
          end
        end
      end

      def wallets_status

        # =begin
        # @api {get} /api/v1/wallets_status?id={:id} 4-Update a Specific User Wallet Status
        # @apiVersion 0.3.0
        # @apiName UpdateWalletsStatus
        # @apiGroup Users Wallets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/wallets_status?id=2
        # @apiParam {Number} id User Wallet unique ID.
        # @apiParam {Number} status User Wallet (0 for Disabled & 1 for Enabled).
        # @apiSuccess {Number} status User Wallet (0 for Disabled & 1 for Enabled).
        # @apiSuccess {Number} id User Wallet unique ID.
        # @apiSuccess {String} currency User Wallet Currency.
        # @apiSuccess {Float} amount User Wallet amount.
        # @apiSuccess {Number} user_id Wallet User ID.
        # @apiSuccess {String} uuid User Wallet Secure random number (Created automatically and must be 12 digits and letters).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #       "id": 2,
        #       "currency": "USD",
        #       "amount": 500,
        #       "user_id": 2,
        #       "status": 0,
        #       "uuid": "0b51db6a5988",
        #       "created_at": "2018-08-02T11:03:52.375Z",
        #       "updated_at": "2018-08-02T13:51:18.179Z"
        #   }
        # @apiError WalletNotFound The id of this User Wallet was not found.  
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "User Wallet Not Found"
        #   }
        # =end

        @wallets_status = UserWallet.where(:id => params[:id].to_i).first
        flash[:error]= "User Wallet Not Found"

        respond_to do |format|
          if @wallets_status.status ==  0
            @wallets_status.update(:status => 1)
            format.json { render json: @wallets_status }
          elsif @wallets_status.status ==  1
            @wallets_status.update(:status => 0)
            format.json { render json: @wallets_status }
          else
            format.json { render json: flash }
          end
        end

      end

      def send_sms

        # =begin
        # @api {get} /api/v1/send_sms?tel={phone_number} 6-Send Verification SMS Code to Users
        # @apiVersion 0.3.0
        # @apiName SendSmsCode
        # @apiGroup Users Wallets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/send_sms?tel=20106455****
        # @apiParam {Number} tel User phone number + country code.
        # @apiSuccess {Number} tel User phone number.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #     "success": " Code was send successfully to number 201064****** "
        #   }
        # @apiError SendCodeFailed Something went wrong while sending SMS code.  
        # @apiErrorExample Error-Response:
        #   {
        #     "error": "Something wrong, Sending code was failed"
        #   }
        # =end

        @phone = params[:tel]
        respond_to do |format|
          if SMSJob.perform_async(tel:@phone)
            flash[:success]= "Code was send successfully to number #{@phone}"
          else
            flash[:error]= "Something wrong, Sending code was failed"
          end
          format.json { render json: flash }
        end
        
    
      end
     
      def verifysms

        # =begin
        # @api {get} /api/v1/verifysms?code={SMS_code} 7-Verify SMS Code
        # @apiVersion 0.3.0
        # @apiName VerifySMSCode
        # @apiGroup Users Wallets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/verifysms?code=3660
        # @apiParam {Number} code The SMS verification code.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #   {
        #     "success": "Valid code, successfully activated"
        #   }
        # @apiError InvalidCode The verification code was wrong.  
        # @apiErrorExample Error-Response:
        #   {
        #     "error": "Wrong code Try again later"
        #   }
        # =end

        @code = params[:code].to_i
        @applications_list = SMSService.new(code:@code).call

        respond_to do |format|
          if @applications_list["verified"] ==  true
            flash[:success]= 'Valid code, successfully activated'
          else 
            flash[:error]=  'Wrong code Try again later'
          end
        format.json { render json: flash }
        end
        
      end

      def transfer_balance
        
        # =begin
        # @api {get} /api/v1/transfer_balance?=&userfrom={:sender_id}&userto={:receiver_id}&transferamount={:amount} 5-Transfer Balances Between Users
        # @apiVersion 0.3.0
        # @apiName TransferBalance
        # @apiGroup Users Wallets
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/transfer_balance?=&userfrom=1&userto=2&transferamount=10
        # @apiParam {Number} userfrom the sender user id.
        # @apiParam {Number} userto the receiver user id.
        # @apiParam {Float} transferamount the amount transferred between users.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #    "success": "Balance was successfully transferred"
        # }
        # @apiErrorExample Error-Response1:
        #   {
        #     "error": "You don't have enough money for this operation"
        #   }
        # @apiErrorExample Error-Response2:
        #   {
        #     "error": "Sorry, The transferd balance must be greater than 2 USD"
        #   }
        # @apiErrorExample Error-Response3:
        #   {
        #     "error": "Sorry, The transferd balance must be less or equal than 10 USD"
        #   }
        # @apiErrorExample Error-Response4:
        #   {
        #     "error": "Sorry, Your Wallet was Disabled"
        #   }
        # @apiErrorExample Error-Response5:
        #   {
        #     "error": "The sender User Wasn't Stored in Our Database"
        #   }
        # @apiErrorExample Error-Response6:
        #   {
        #     "error": "The receiver User Wasn't Stored in Our Database"
        #   }
        # @apiErrorExample Error-Response6:
        #   {
        #     "error": "Sorry, The Receiver User Wallet was Disabled"
        #   }
        # @apiErrorExample Error-Response8:
        #   {
        #     "error": "You can't transfer balance to yourself"
        #   }
        # =end

        @user_from = params[:userfrom]
        @user_to = params[:userto]
        @transfer_amount = params[:transferamount]
        @message = UserWallet.checktransfer(@user_from.to_i,@user_to.to_i,@transfer_amount)
        respond_to do |format|
          if @message == ""
            ActiveRecord::Base.transaction do
              @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).lock('LOCK IN SHARE MODE').first
              @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).lock('LOCK IN SHARE MODE').first
              @new_balance_from = @user_wallet_from.amount - @transfer_amount.to_f
              @new_balance_to = @user_wallet_to.amount + @transfer_amount.to_f
              @user_wallet_from.update(:amount => @new_balance_from )
              @user_wallet_to.update(:amount => @new_balance_to )
              @smstext = "#{@transfer_amount} USD has been successfully transferred to user #{@user_to}"
              @tel = params[:tel]
              @user_mail = params[:email]
              @user_notification_setting = NotificationsSetting.where(user_id: @user_from).first
              if @user_notification_setting.money_transactions != 0
                @aa = Notification.create(user_id: @user_from ,title: "Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions)
              end
              if @user_notification_setting.money_transactions == 3
                SMSNotification.sms_notification_setting(@tel,@smstext)
                EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
              elsif @user_notification_setting.money_transactions == 2
                SMSNotification.sms_notification_setting(@tel,@smstext)
              elsif @user_notification_setting.money_transactions == 1
                EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
              end
            end
            flash[:success]= "Balance was successfully transferred"
            format.json { render json: flash }
          else
            flash[:error]= @message
            format.json { render json: flash }
          end
        end
      end

      def transfer_balancepost
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_user_wallet
          @user_wallet = UserWallet.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def user_wallet_params
          params.permit(:currency, :amount, :user_id, :status, :uuid)
        end
    end
  end
end
