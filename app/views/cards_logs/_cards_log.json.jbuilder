json.extract! cards_log, :id, :user_id, :action_type, :ip, :created_at, :updated_at
json.url cards_log_url(cards_log, format: :json)
