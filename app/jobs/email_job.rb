# app/jobs/email_job.rb

class EmailJob

    include SuckerPunch::Job

    # Email Provider Background Job
    # @param [String] site_signature the sender email address(payers@payers.com).
    # @param [Integer] user_mail The receiver user email address.
    # @param [String] subject The subject of the email.
    # @param [String] text The email content text.
    # @param [String] EmailService_Provider The Email provider name.
    def perform(user_mail:'client@payers.com',subject:'Welcome To Payers',text:'Welcome',via:'payers@payers.com',email_provider:'mailgun')     
        
        @user_mail = user_mail
        @subject = subject
        @text = text
        @via = via
        @EmailService_Provider = email_provider

        EmailService.new(user_mail:@user_mail,subject:@subject,text:@text,via:@via,email_provider:@EmailService_Provider).call
    
    end

end