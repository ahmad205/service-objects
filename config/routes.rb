Rails.application.routes.draw do
  
  resources :notifications
  resources :addresses
  resources :uploads
  resources :translations
  resources :cards_logs
  resources :media
  resources :pages
  resources :sms_logs
  resources :notifications_settings
  resources :wallets_transfer_ratios
  resources :user_wallets
  resources :money_ops
  resources :cards_categories
  resources :cards
  get "search" => "cards#search"
  post "search" => "cards#searchpost"
  get "buy_card" => "cards#buy_card" 
  get "cards_status" => "cards#cards_status"
  get "display_cards_categories" => "cards_categories#display_cards_categories"
  get "operations_status" => "money_ops#operations_status" 
  get "wallets_status" => "user_wallets#wallets_status"
  get "transfer_balance" => "user_wallets#transfer_balance"
  post "transfer_balance" => "user_wallets#transfer_balancepost"
  get "add_defualt_notifications" => "notifications_settings#add_defualt_notifications"
  get "send_sms" => "user_wallets#send_sms"
  post "send_sms_post" => "user_wallets#send_sms_post"
  post "verifysms_post" => "user_wallets#verifysms_post"
  get "verifysms" => "user_wallets#verifysms"
  get "/sitemap.xml" => "pages#sitemap", :format => "xml", :as => :sitemap
  get "facebook" => "media#facebook"
  post "facebook_post" => "media#facebook_post"
  get "change_locale" => "pages#change_locale"
  get "user_addresses" => "addresses#user_addresses"
  get "search_address" => "addresses#search"
  post "search_address" => "addresses#searchpost"
  get "delete_user_addresses" => "addresses#delete_user_addresses"
  get "user_notifications" => "notifications#user_notifications"
  get "search_notifications" => "notifications#search"


  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :cards
      get "search" => "cards#search"
      post "search" => "cards#searchpost"
      get "buy_card" => "cards#buy_card"
      get "cards_status" => "cards#cards_status"
      resources :cards_categories
      get "display_cards_categories" => "cards_categories#display_cards_categories"
      resources :money_ops
      get "operations_status" => "money_ops#operations_status"
      resources :user_wallets
      get "wallets_status" => "user_wallets#wallets_status"
      get "transfer_balance" => "user_wallets#transfer_balance"
      post "transfer_balance" => "user_wallets#transfer_balancepost"
      get "send_sms" => "user_wallets#send_sms"
      post "send_sms_post" => "user_wallets#send_sms_post"
      post "verifysms_post" => "user_wallets#verifysms_post"
      get "verifysms" => "user_wallets#verifysms"
      resources :notifications_settings
      get "add_defualt_notifications" => "notifications_settings#add_defualt_notifications"
      resources :sms_logs
      resources :pages
      resources :cards_logs
      resources :addresses
      resources :uploads
      resources :translations
      get "user_addresses" => "addresses#user_addresses"
      get "search_address" => "addresses#search"
      post "search_address" => "addresses#searchpost"
      get "delete_user_addresses" => "addresses#delete_user_addresses"
      resources :notifications
      get "user_notifications" => "notifications#user_notifications"
    end
  end
  
end
