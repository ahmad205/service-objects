require "rails_helper"

RSpec.describe CardsLogsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/cards_logs").to route_to("cards_logs#index")
    end

    it "routes to #new" do
      expect(:get => "/cards_logs/new").to route_to("cards_logs#new")
    end

    it "routes to #show" do
      expect(:get => "/cards_logs/1").to route_to("cards_logs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/cards_logs/1/edit").to route_to("cards_logs#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/cards_logs").to route_to("cards_logs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/cards_logs/1").to route_to("cards_logs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/cards_logs/1").to route_to("cards_logs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/cards_logs/1").to route_to("cards_logs#destroy", :id => "1")
    end
  end
end
