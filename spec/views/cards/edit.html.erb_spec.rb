require 'rails_helper'

RSpec.describe "cards/edit", type: :view do
  before(:each) do
    @card = assign(:card, Card.create!(
      :number => 1,
      :value => 1,
      :status => 1,
      :expired_at => "",
      :user_id => 1,
      :invoice_id => 1
    ))
  end

  it "renders the edit card form" do
    render

    assert_select "form[action=?][method=?]", card_path(@card), "post" do

      assert_select "input[name=?]", "card[number]"

      assert_select "input[name=?]", "card[value]"

      assert_select "input[name=?]", "card[status]"

      assert_select "input[name=?]", "card[expired_at]"

      assert_select "input[name=?]", "card[user_id]"

      assert_select "input[name=?]", "card[invoice_id]"
    end
  end
end
