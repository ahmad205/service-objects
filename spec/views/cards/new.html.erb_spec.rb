require 'rails_helper'

RSpec.describe "cards/new", type: :view do
  before(:each) do
    assign(:card, Card.new(
      :number => 1,
      :value => 1,
      :status => 1,
      :expired_at => "",
      :user_id => 1,
      :invoice_id => 1
    ))
  end

  it "renders new card form" do
    render

    assert_select "form[action=?][method=?]", cards_path, "post" do

      assert_select "input[name=?]", "card[number]"

      assert_select "input[name=?]", "card[value]"

      assert_select "input[name=?]", "card[status]"

      assert_select "input[name=?]", "card[expired_at]"

      assert_select "input[name=?]", "card[user_id]"

      assert_select "input[name=?]", "card[invoice_id]"
    end
  end
end
