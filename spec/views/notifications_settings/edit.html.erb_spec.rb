require 'rails_helper'

RSpec.describe "notifications_settings/edit", type: :view do
  before(:each) do
    @notifications_setting = assign(:notifications_setting, NotificationsSetting.create!(
      :user_id => 1,
      :money_transactions => 1,
      :pending_transactions => 1,
      :transactions_updates => 1,
      :help_tickets_updates => 1,
      :tickets_replies => 1,
      :account_login => 1,
      :change_password => 1,
      :verifications_setting => 1
    ))
  end

  it "renders the edit notifications_setting form" do
    render

    assert_select "form[action=?][method=?]", notifications_setting_path(@notifications_setting), "post" do

      assert_select "input[name=?]", "notifications_setting[user_id]"

      assert_select "input[name=?]", "notifications_setting[money_transactions]"

      assert_select "input[name=?]", "notifications_setting[pending_transactions]"

      assert_select "input[name=?]", "notifications_setting[transactions_updates]"

      assert_select "input[name=?]", "notifications_setting[help_tickets_updates]"

      assert_select "input[name=?]", "notifications_setting[tickets_replies]"

      assert_select "input[name=?]", "notifications_setting[account_login]"

      assert_select "input[name=?]", "notifications_setting[change_password]"

      assert_select "input[name=?]", "notifications_setting[verifications_setting]"
    end
  end
end
