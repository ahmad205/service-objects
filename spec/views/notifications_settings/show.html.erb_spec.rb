require 'rails_helper'

RSpec.describe "notifications_settings/show", type: :view do
  before(:each) do
    @notifications_setting = assign(:notifications_setting, NotificationsSetting.create!(
      :user_id => 2,
      :money_transactions => 3,
      :pending_transactions => 4,
      :transactions_updates => 5,
      :help_tickets_updates => 6,
      :tickets_replies => 7,
      :account_login => 8,
      :change_password => 9,
      :verifications_setting => 10
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(/7/)
    expect(rendered).to match(/8/)
    expect(rendered).to match(/9/)
    expect(rendered).to match(/10/)
  end
end
