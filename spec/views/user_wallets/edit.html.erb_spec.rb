require 'rails_helper'

RSpec.describe "user_wallets/edit", type: :view do
  before(:each) do
    @user_wallet = assign(:user_wallet, UserWallet.create!(
      :currency => "MyString",
      :amount => "MyString",
      :user_id => 1,
      :status => 1,
      :uuid => "MyString"
    ))
  end

  it "renders the edit user_wallet form" do
    render

    assert_select "form[action=?][method=?]", user_wallet_path(@user_wallet), "post" do

      assert_select "input[name=?]", "user_wallet[currency]"

      assert_select "input[name=?]", "user_wallet[amount]"

      assert_select "input[name=?]", "user_wallet[user_id]"

      assert_select "input[name=?]", "user_wallet[status]"

      assert_select "input[name=?]", "user_wallet[uuid]"
    end
  end
end
