require 'rails_helper'

RSpec.describe "money_ops/edit", type: :view do
  before(:each) do
    @money_op = assign(:money_op, MoneyOp.create!(
      :opid => "MyString",
      :optype => 1,
      :amount => "MyString",
      :payment_gateway => 1,
      :status => 1,
      :payment_id => 1,
      :user_id => 1
    ))
  end

  it "renders the edit money_op form" do
    render

    assert_select "form[action=?][method=?]", money_op_path(@money_op), "post" do

      assert_select "input[name=?]", "money_op[opid]"

      assert_select "input[name=?]", "money_op[optype]"

      assert_select "input[name=?]", "money_op[amount]"

      assert_select "input[name=?]", "money_op[payment_gateway]"

      assert_select "input[name=?]", "money_op[status]"

      assert_select "input[name=?]", "money_op[payment_id]"

      assert_select "input[name=?]", "money_op[user_id]"
    end
  end
end
