require 'rails_helper'

RSpec.describe "cards_logs/index", type: :view do
  before(:each) do
    assign(:cards_logs, [
      CardsLog.create!(
        :user_id => 2,
        :action_type => 3,
        :ip => "Ip"
      ),
      CardsLog.create!(
        :user_id => 2,
        :action_type => 3,
        :ip => "Ip"
      )
    ])
  end

  it "renders a list of cards_logs" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Ip".to_s, :count => 2
  end
end
