require 'rails_helper'

RSpec.describe "cards_logs/show", type: :view do
  before(:each) do
    @cards_log = assign(:cards_log, CardsLog.create!(
      :user_id => 2,
      :action_type => 3,
      :ip => "Ip"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Ip/)
  end
end
