require 'rails_helper'

RSpec.describe "addresses/edit", type: :view do
  before(:each) do
    @address = assign(:address, Address.create!(
      :user_id => 1,
      :address => "MyString",
      :country => "MyString",
      :governorate => "MyString",
      :city => "MyString",
      :street => "MyString",
      :default_address => false
    ))
  end

  it "renders the edit address form" do
    render

    assert_select "form[action=?][method=?]", address_path(@address), "post" do

      assert_select "input[name=?]", "address[user_id]"

      assert_select "input[name=?]", "address[address]"

      assert_select "input[name=?]", "address[country]"

      assert_select "input[name=?]", "address[governorate]"

      assert_select "input[name=?]", "address[city]"

      assert_select "input[name=?]", "address[street]"

      assert_select "input[name=?]", "address[default_address]"
    end
  end
end
