require 'rails_helper'

RSpec.describe "pages/index", type: :view do
  before(:each) do
    assign(:pages, [
      Page.create!(
        :order => 2,
        :active => 3,
        :content => "Content",
        :title => "Title",
        :slug => "Slug",
        :description => "Description",
        :keywords => "Keywords",
        :admin_id => 4
      ),
      Page.create!(
        :order => 2,
        :active => 3,
        :content => "Content",
        :title => "Title",
        :slug => "Slug",
        :description => "Description",
        :keywords => "Keywords",
        :admin_id => 4
      )
    ])
  end

  it "renders a list of pages" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Content".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Slug".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Keywords".to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
