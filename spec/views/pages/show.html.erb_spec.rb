require 'rails_helper'

RSpec.describe "pages/show", type: :view do
  before(:each) do
    @page = assign(:page, Page.create!(
      :order => 2,
      :active => 3,
      :content => "Content",
      :title => "Title",
      :slug => "Slug",
      :description => "Description",
      :keywords => "Keywords",
      :admin_id => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Content/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Slug/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/Keywords/)
    expect(rendered).to match(/4/)
  end
end
