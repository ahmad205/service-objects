require 'rails_helper'

RSpec.describe "cards_categories/show", type: :view do
  before(:each) do
    @cards_category = assign(:cards_category, CardsCategory.create!(
      :card_value => 2,
      :active => 3,
      :order => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
  end
end
