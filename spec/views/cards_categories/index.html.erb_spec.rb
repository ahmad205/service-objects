require 'rails_helper'

RSpec.describe "cards_categories/index", type: :view do
  before(:each) do
    assign(:cards_categories, [
      CardsCategory.create!(
        :card_value => 2,
        :active => 3,
        :order => 4
      ),
      CardsCategory.create!(
        :card_value => 2,
        :active => 3,
        :order => 4
      )
    ])
  end

  it "renders a list of cards_categories" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
