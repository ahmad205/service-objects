require 'rails_helper'

RSpec.describe "media/edit", type: :view do
  before(:each) do
    @medium = assign(:medium, Medium.create!(
      :admin_id => 1,
      :media_type => "MyString",
      :post_content => "MyString"
    ))
  end

  it "renders the edit medium form" do
    render

    assert_select "form[action=?][method=?]", medium_path(@medium), "post" do

      assert_select "input[name=?]", "medium[admin_id]"

      assert_select "input[name=?]", "medium[media_type]"

      assert_select "input[name=?]", "medium[post_content]"
    end
  end
end
