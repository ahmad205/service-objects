require 'rails_helper'

RSpec.describe "media/index", type: :view do
  before(:each) do
    assign(:media, [
      Medium.create!(
        :admin_id => 2,
        :media_type => "Media Type",
        :post_content => "Post Content"
      ),
      Medium.create!(
        :admin_id => 2,
        :media_type => "Media Type",
        :post_content => "Post Content"
      )
    ])
  end

  it "renders a list of media" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Media Type".to_s, :count => 2
    assert_select "tr>td", :text => "Post Content".to_s, :count => 2
  end
end
