require 'rails_helper'

RSpec.describe "media/new", type: :view do
  before(:each) do
    assign(:medium, Medium.new(
      :admin_id => 1,
      :media_type => "MyString",
      :post_content => "MyString"
    ))
  end

  it "renders new medium form" do
    render

    assert_select "form[action=?][method=?]", media_path, "post" do

      assert_select "input[name=?]", "medium[admin_id]"

      assert_select "input[name=?]", "medium[media_type]"

      assert_select "input[name=?]", "medium[post_content]"
    end
  end
end
