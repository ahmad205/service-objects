class AddCardIdToCardsLogs < ActiveRecord::Migration[5.2]
  def change
    add_column :cards_logs, :card_id, :integer
  end
end
