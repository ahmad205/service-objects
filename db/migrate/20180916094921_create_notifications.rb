class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.string :title
      t.string :description
      t.integer :notification_type

      t.timestamps
    end
  end
end
