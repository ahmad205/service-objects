class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.integer :order
      t.integer :active
      t.string :content
      t.string :title
      t.string :slug
      t.string :description
      t.string :keywords
      t.integer :admin_id

      t.timestamps
    end
  end
end
