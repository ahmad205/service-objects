class CreateMoneyOps < ActiveRecord::Migration[5.2]
  def change
    create_table :money_ops do |t|
      t.string :opid
      t.integer :optype
      t.float :amount , :limit=>53 
      t.integer :payment_gateway
      t.integer :status
      t.timestamp :payment_date
      t.integer :payment_id
      t.integer :user_id

      t.timestamps
    end
  end
end
