class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.integer :user_id
      t.string :address
      t.string :country
      t.string :governorate
      t.string :city
      t.string :street
      t.boolean :default_address

      t.timestamps
    end
  end
end
