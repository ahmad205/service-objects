class CreateCardsLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :cards_logs do |t|
      t.integer :user_id
      t.integer :action_type
      t.string :ip

      t.timestamps
    end
  end
end
