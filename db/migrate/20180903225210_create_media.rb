class CreateMedia < ActiveRecord::Migration[5.2]
  def change
    create_table :media do |t|
      t.integer :admin_id
      t.string :media_type
      t.string :post_content

      t.timestamps
    end
  end
end
